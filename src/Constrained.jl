using Transducers
using BlackBoxOptim
using Plots
import Base.Threads.@threads
import Base.@lock

# growth params
attrition = .1
growth_decline = .1
steady_growth = .2
# max growth --> g
# max drawdown --> d

# params
init_policies = 20000
cashflow = 400
nMonths = 84
policyduration = 72
totalMarket = 3600000
max_marketshare = .15


struct ModelStep
    month::Integer
    total_policies::Integer
    new_policies::Integer
    revenue::Float64
    spend::Float64
    net::Float64
    pnl::Float64
end
ModelStep(init=init_policies) = ModelStep(
    0,
    init,
    0,
    0,
    0,
    0,
    0
)

struct Model
    steps::Vector{ModelStep}
end

max_new_policies_filt(ii::Float64) = max(min(ii, 10e17), 0)
max_new_policies(ms::ModelStep, burn_factor::Real, cost::Real, burn_limit::Real) = (ms.pnl + burn_limit) * burn_factor / cost |> max_new_policies_filt

payoutstream(cf, attr, pdur) = vcat([cf],[0 for i=1:11],[cf / 12 * (1 - attr/12)^(i-1) for i=13:pdur])

payout(n::Integer, ps::Vector) = n > length(ps) ? sum(ps) : sum(ps[1:n])

growth(n, g) = max(steady_growth, g - growth_decline * n/12)

function new_policies_step(ms::ModelStep, burn_factor::Real, g::Real, cost::Real, burn_limit::Real)
    g_ann = max(steady_growth, g - ms.month*growth_decline/12)
    np_raw = g_ann / 12 * ms.total_policies
    np_max1 = max_new_policies(ms, burn_factor, cost, burn_limit)
    np_max2 = totalMarket / 12 * max_marketshare
    max(min(np_raw, np_max1, np_max2),0) |> round |> Integer
end

# g is the initial growth rate

ps = payoutstream(cashflow, attrition, policyduration)

model(g::Real; burn_factor::Real, cost::Real, burn_limit::Real) = 1:nMonths |> Iterated(x-> step(x, burn_factor, g, cost, burn_limit), ModelStep()) |> collect |> Model

function step(ms::ModelStep, burn_factor::Real, max_growth::Real, cost::Real, burn_limit::Real)
    annualized_growth = growth(ms.month, max_growth)
    new_policies= new_policies_step(ms, burn_factor, max_growth, cost, burn_limit)
    revenue = round(new_policies * payout(ms.month + 1, ps), digits=2)
    spend = round(cost * new_policies, digits=2)
    burn_pct = spend / ms.pnl
    net = round(revenue - spend, digits=2)
    pnl = round(net + ms.pnl, digits=2)
    ModelStep(
        ms.month + 1,
        ms.total_policies + new_policies,
        new_policies,
        revenue,
        spend,
        net,
        pnl
    )
end

drawdown(m::Vector{T}, xcount) where T<:Real = minimum(m[1:xcount])
drawdown(m::Vector{T}) where T<:Real = minimum(m)
drawdown(m::Model, args...) = drawdown([x.pnl for x in m.steps], args...)

dcounter(m::Vector{T}, xcount) where T<: Real = m[xcount-10:xcount+1] |> sum
dcounter(m::Vector{T}) where T<: Real = dcounter(m, length(m))
ebitda(m::Model, args...) = dcounter([x.net for x in m.steps], args...)
revenue(m::Model, args...) = dcounter([x.revenue for x in m.steps], args...)

function get_obj(func::Function; max_growth, cost, burn_limit)
    dd = abs(burn_limit)
    function pf(x)
        m = model(max_growth; burn_factor=x, cost=cost, burn_limit=burn_limit)
        obj = 1/func(m)
        draw = drawdown(m)
        minpen = 1.0
        K=10e9
        penalty = abs(draw) < dd ? 0 : minpen + (abs(draw) - dd) * K
        obj + penalty
    end
    pf(v::Vector) = pf(v[1])
    pf
end

function gridsearch(func::Function; max_growth, cost, burn_limit, xrange=.0:.01:2)
    mgen(x) = model(max_growth; burn_factor = x, cost=cost, burn_limit=burn_limit)
    fout, rout, dout = (zeros(length(xrange)) for _=1:3)
    @threads for i=1:length(xrange)
        x=xrange[i]
        m = mgen(x)
        d = drawdown(m)
        fout[i] = x
        dout[i] = d
        rout[i] = abs(d) < burn_limit ? func(m) : 0
    end
    fout, rout, dout
end

function gridres(; max_growth, burn_limit, costrange=300:5:1200)
    x, y, z = (zeros(length(costrange)) for _=1:3)
    func = x->ebitda(x, 60)
    for (i,cost) in enumerate(costrange)
        x[i] = cost
        fout, rout, _ = gridsearch(func; max_growth=max_growth, cost=cost, burn_limit=burn_limit)
        z[i], imax = findmax(rout)
        y[i] = fout[imax]
    end
    x,y,z
end

function blplot(;max_growth, costrange=300:5:1200, lims=10:10:50)
    p = plot(size=(1200,900), title=string("Cost Impact with Constraint -- g=", max_growth), xlabel="Cost / Policy")
    for (i,lim) in enumerate(lims)
        x,_,z = gridres(max_growth=max_growth, costrange=costrange, burn_limit=lim*10^6)
        plot!(x,z, label=string(lim,"m Limit"))
    end
    p
end


function opt(func::Function; max_growth, kwargs...)
    @show kwargs
    f = get_obj(func; max_growth=max_growth, kwargs...)
    res = bboptimize(f; SearchRange=[(0,2)], Method=:generating_set_search)
    x = res.archive_output.best_candidate[1]
    m = model(max_growth; burn_factor=x, kwargs...)
    res = func(m)
    @show x
    @show res
    return x,res,m
end


function get_outcome_surface(cc=400:100:1200, dd=10:2:20)
    func = x-> ebitda(x, 60)
    gout = Dict()
    eout = Dict()
    @threads for i=1:length(cc)
        cost = cc[i]
        for (j,mdr) in enumerate(dd)
            md = mdr * 10^6
            g,d = opt(func, cost = cost, max_drawdown = md)
            gout[(cost, mdr)] = g
            eout[(cost, mdr)] = d
        end
    end
    gout, eout
end

function plot_cost_out(cc=400:5:1000, max_drawdown=10e6)
    func = x-> ebitda(x,60)
    gout = zeros(length(cc))
    eout = zeros(length(cc))
    @threads for i=1:length(cc)
        cost = cc[i]
        g,d = opt(func, cost=cost, max_drawdown=max_drawdown)
        gout[i] = g
        eout[i] = d
    end
    gout, eout
end

plot_outcome_surface(xx, yy, zdic) = surface(xx, yy, (x,y)->zdic[(x,y)], size=(1200,900))

function solve_target(target_ebitda; cost, months, step=.001, range=(0,3))
    o1 = 0
    o2 = 0
    g1 = 0
    g2 = 0
    for i=range[1]:step:range[2]
        m = model(i; cost=cost)
        x = ebitda(m, months)
        o2 = x
        g2 = i
        if o2 > target_ebitda
            d1 = target_ebitda - o1
            d2 = o2 - target_ebitda
            return d1 < d2 ? (g1, o1) : (g2, o2)
        end
        o1 = o2
        g1 = g2
    end
    g2, o2
end

function plot_growth_tpolicies(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    @threads for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        ys[i] = (m[1][1:months] |> totalPolicies)/1e3
    end
    p = plot(xlabel = "Months", ylabel = "Total Policies - Thousands", title="Effect of Growth Rate - Total Policies")
    for (i,g) in enumerate(growth)
        plot!(ys[i], size=(1200,900), label=g)
    end
    p
end

function plot_growth_npolicies(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    @threads for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        ys[i] = (m[1][1:months])/1e3
    end
    p = plot(xlabel = "Months", ylabel = "New Policies", title="Effect of Growth Rate - Thousands of New Policies / Month")
    for (i,g) in enumerate(growth)
        plot!(ys[i], size=(1200,900), label=g)
    end
    p
end

function plot_growth_ebitda(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        p = m[4][1:months] |> Partition(12, step=1) |> Map(v -> sum(v)/1e6) |> collect
        ys[i] = vcat(zeros(12), p)
    end
    @show ys[1] |> length
    p = plot(xlabel = "Months", title="Effect of Growth Rate - EBITDA", xticks=0:12:months)
    for (i,g) in enumerate(growth)
        lw = .77 < g ≤ .8 ? 4 : 1
        plot!(0:(length(ys[i])-1),ys[i], size=(1200,900), label=g, linewidth=lw)
    end
    p
end

function plot_growth_revenue(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    @threads for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        ys[i] = m[2][1:months] |> Partition(12, step=1) |> Map(v -> sum(v)/1e6) |> collect
    end
    p = plot(xlabel = "Months", title="Effect of Growth Rate - Revenue")
    for (i,g) in enumerate(growth)
        lw = .77 < g ≤ .8 ? 4 : 1
        println(lw)
        plot!(12:(length(ys[i])+11),ys[i], size=(1200,900), label=g, linewidth=lw)
    end
    p
end

function plot_growth_drawdown(; cost, months, growth)
    n = length(growth)
    nn = length(cost)
    ys = [zeros(nn) for g in growth]
    @threads for i=1:n
        for j=1:nn
            g = growth[i]
            c = cost[j]
            m = model(g; cost=c)
            ys[i][j] = drawdown(m, months) / 1e6
        end
    end
    title = string("Effects of Growth Rate and Cost - Max Drawdown - ", months / 12, " Years")
    p = plot(xlabel = "Cost", ylabel = "Max Drawdown - Millions", title=title, size=(1200,900))
    for i=1:n
        lw = growth[i] == .785 ? 4 : :auto
        plot!(cost, ys[i], label=growth[i], seriestype=:path, linewidth=lw)
    end
    vline!([550], color=:red, linewidth=3, label="")
    p
end

function plot_growth_EBITDA(; cost, months, growth)
    n = length(growth)
    nn = length(cost)
    ys = [zeros(nn) for g in growth]
    @threads for i=1:n
        for j=1:nn
            g = growth[i]
            c = cost[j]
            m = model(g; cost=c)
            ys[i][j] = ebitda(m, months) / 1e6
        end
    end
    title = string("Effects of Growth Rate and Cost - EBITDA - ", months / 12, " Years")
    p = plot(xlabel = "Cost", ylabel = "EBITDA - Millions", title = title, size=(1200,900))
    for i=1:n
        lw = .77 < growth[i] <= .8 ? 4 : 1
        plot!(cost, ys[i], label=growth[i], linewidth=lw)
    end
    vline!([550], color=:red, linewidth=3, label="")
    p
end

s(args...;zid, months, kwargs...) = s(gen_grid(xcount=months); zid=zid, kwargs...)
function s(gr::Tuple; zid, kwargs...)
    @assert zid in (1,2,3)
    x,y,z1,z2,z3 = gr
    zfunc = (z1,z2,z3)[zid]
    z(args...) = zfunc(args...) / 1e6
    surf = surface(x,y,z, size=(1200,900); kwargs...)
end

function s1(args...; months, kwargs...)
    title = string("Cost, Growth, ", months/12,"yr EBITDA")
    ss = s(args...; months=months, zid=1, xlabel="cost", ylabel="growth", zlabel="EBITDA in millions", title=title, kwargs...)

end

function s3(args...; months, kwargs...)
    title = string("Cost, Growth, Max Drawdown")
    s(args...; months=months, zid=3, xlabel="cost", ylabel="growth", zlabel="drawdown", title=title, kwargs...)
end
