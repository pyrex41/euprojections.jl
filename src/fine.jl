using Transducers, NLopt

abstract type Model end

mutable struct MonthModel <: Model
    cash::Real
    month::Integer
    delta::Dict{Int, Real}
    revenue_log::Dict{Int, Real}
    cac_spend::Dict{Int, Real}
    commission_spend::Dict{Int, Real}
    overhead_spend::Dict{Int, Real}
    active_policies::Dict{Int, Int}
    inactive_policies::Dict{Int, Int}
    total_policies::Dict{Int, Int}
end
zdic(n) = Dict(i=>0 for i=1:n)
function MonthModel(init, cash, n_months)
    m = MonthModel(
        cash,
        1,
        zdic(n_months),
        zdic(n_months),
        zdic(n_months),
        zdic(n_months),
        zdic(n_months),
        zdic(n_months),
        zdic(n_months),
        zdic(n_months)
    )
    m.active_policies[1] = init
    m.total_policies[1] = init
    m
end

attrition = .1 # annual
first_year_policies = 20000 # .17% market share
starting_annual_growth = 2 #
growth_rate_decrease = .2 # annualized
avg_cashflow = 400 # $600 /year
duration = 6 * 12 # 6 years
n_months = 4 * 12
cac_cost = 500
overhead_per_policy = 100
commission_per_policy = 150
total_market_size = 3600000

MonthModel() = MonthModel(first_year_policies, 0, n_months)

simple_grow(n) = max(.1/12, starting_annual_growth/12 * (1 - growth_rate_decrease / 12)^n)

function step_month(model::MonthModel, growth::Function=simple_grow)
    n = model.month

    # reduce from attrition
    af = attrition / 12

    model.delta[n+1] = 0

    #residuals
    model.revenue_log[n+1] = 0
    i0=max(1, n-duration)
    for i=i0:n
        # cancel attrition policies
        cancelled_policies = model.active_policies[i] * af |> round
        model.active_policies[i] = model.active_policies[i] - cancelled_policies
        model.inactive_policies[i] = get(model.inactive_policies, i, 0) + cancelled_policies

        # get_residual_cash
        if duration > n - i ≥ 12
            residual_payout = avg_cashflow / 12
            residual_cash = model.active_policies[i] * residual_payout
            model.delta[n+1] += residual_cash
            model.revenue_log[n+1] += residual_cash
        end
    end

    # new_policies
    total_policies = 1:n |> Map(x-> model.active_policies[x] + model.inactive_policies[x]) |> sum
    monthly_policies = growth(n) * total_policies |> round
    model.active_policies[n+1] = monthly_policies
    model.revenue_log[n+1] += monthly_policies * avg_cashflow
    model.commission_spend[n+1] = monthly_policies * commission_per_policy
    model.delta[n+1] += monthly_policies * (avg_cashflow - commission_per_policy)
    model.total_policies[n+1] = total_policies + monthly_policies
    cac_spend= monthly_policies * cac_cost
    model.cac_spend[n+1] = cac_spend
    model.delta[n+1] -= cac_spend

    #overhead
    overhead_cost = monthly_policies * overhead_per_policy
    model.overhead_spend[n+1] = overhead_cost
    model.delta[n+1] -= overhead_cost

    model.cash += model.delta[n+1]
    model.month += 1
    return model
end

vdic(dic) = [dic[i] for i=1:length(dic)]

function r()
    m = MonthModel()
    for i=1:n_months
        step_month(m)
    end
    m
end

function pr(m::MonthModel, out="out.png")
    y3 = vdic(m.total_policies) / total_market_size / 12
    y1 = vdic(m.revenue_log) / 1e6 |> Consecutive(12, step=1) |> Map(sum) |> collect
    y2 = vdic(m.delta)/1e6 |> Consecutive(12, step=1) |> Map(sum) |> collect

    plot(y3, label="Market Share", ylabel="fraction", color=:red, size=(1200, 900))
    plot!(twinx(), [y1,y2], label=["Revenue" "Earnings"], xlabel="months", ylabel="millions", legend=:bottomright)
    savefig(out)

end

function func
