module EUProjections


export newPoliciesStep, growth, payoutstream, payout, totalPolicies, model, npv, drawdown, objective,
    get_obj, gen_grid, opt, ebitda, solve_target, plot_growth_tpolicies, plot_growth_ebitda,
    plot_growth_npolicies, plot_growth_drawdown, plot_growth_EBITDA, s1, s3, revenue, plot_growth_revenue,
    plot_outcome_surface, plot_cost_out, gridsearch, gridres, blplot


#include("BoE.jl")
#include("Monthly.jl")
#Write your package code here.
include("Constrained.jl")

end
