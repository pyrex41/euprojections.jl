using Transducers
using BlackBoxOptim
using Plots
import Base.Threads.@threads
import Base.@lock

# growth params
attrition = .1
growthdecline = .1
steadygrowth = .2
cac = 300
overhead = 200
commission = 150
cost() = cac + overhead + commission

# params
init_policies = 20000
cashflow = 400
nMonths = 120
policyduration = 72
totalMarket = 3600000
max_marketshare = .15

payoutstream(cf, attr, pdur) = vcat([cf],[0 for i=1:11],[cf / 12 * (1 - attr/12)^(i-1) for i=13:pdur])

payout(n::Integer, ps::Vector) = n > length(ps) ? sum(ps) : sum(ps[1:n])

growth(n, g) = max(steadygrowth, g - growthdecline * n/12)

newPoliciesHelp(last, growthrate, min_) = last + round(min(last * growthrate/12, min_))
newPoliciesStep(last, n, g) = newPoliciesHelp(last, growth(n-1, g), totalMarket * max_marketshare / 12) |> Integer

totalPolicies(g::Real) = vcat([init_policies],collect(1:nMonths |> Scan((x,y) -> newPoliciesStep(x,y,g), init_policies)))
totalPolicies(v::Vector, init=init_policies) = v |> Scan(+, init) |> collect
# g is the initial growth rate
function model(g; cost)
    ps = payoutstream(cashflow, attrition, policyduration)
    np = g |> totalPolicies |> diff
    revenue = 1:length(np) |> Map(i-> np[i] * payout(i, ps)) |> tcollect
    spend = cost * np
    net = revenue .- spend
    pnl = net |> Scan(+, 0) |> collect
    np, revenue, spend, net, pnl
end

npv(m::Vector{T}, d, xcount) where T<:Real = foldxt(+, 1:xcount |> Map(i -> m[i] * (1 - d/12)^i))
npv(m::Vector{T}, d) where T<:Real =foldxt(+,1:length(m) |> Map(i-> m[i] * (1 - d/12)^i))
npv(m, args...) = npv(m[4], args...)

drawdown(m::Vector{T}, xcount) where T<:Real = minimum(m[1:xcount])
drawdown(m::Vector{T}) where T<:Real = minimum(m)
drawdown(m, args...) = drawdown(m[5], args...)

dcounter(m::Vector{T}, xcount) where T<: Real = m[xcount-10:xcount+1] |> sum
dcounter(m::Vector{T}) where T<: Real = dcounter(m, length(m))
ebitda(m, args...) = dcounter(m[4], args...)
revenue(m, args...) = dcounter(m[2], args...)

function get_obj(dd_limit; n, cost)
    dd = abs(dd_limit)
    function pf(x)
        m = model(x; cost=cost)
        obj = 1/ebitda(m, n)
        draw = drawdown(m)
        minpen = 1.0
        K=1e6
        penalty = abs(draw) < dd ? 0 : minpen + (abs(draw) - dd) * K
        obj + penalty
    end
    pf(v::Vector) = pf(v[1])
    pf
end

function opt(;max_drawdown, cost, n=60)
    f = get_obj(max_drawdown; n=n, cost=cost)
    res = bboptimize(f; SearchRange=[(0,3)])
    g = res.archive_output.best_candidate[1]
    d = ebitda(model(g, cost=cost), n)
    @show g
    @show d
    return g,d
end
function twoDplot1(gr)
    p = plot(title="5 Year EBITD in Millions", xlabel="Cost / Policy", size=(1200,900))
    x,_,z1,_ = gr
    for g in .8:.1:1.1
        plot!(x, [z1(i, g)/1e6 for i in x], label=string("growth = ", g))
    end
    vline!([550], color=:red, linewidth=3, label="")
    p
end
function twoDplot2(gr)
    p = plot(title="5 Year EBITDA in Millions", xlabel="Growth Rate", size=(1200,900))
    _,y,z1,_ = gr
    for c in 350:200:950
        lw = c == 550 ? 4 : 1
        plot!(y, [z1(c, j)/1e6 for j in y], label=string("cost = ", c), linewidth=lw)
    end
    vline!([.8], color=:red, linewidth=3, label="")
    p
end

function opt_grid(dd; l = 350, step=10, u=1200, n=60)
    ii = l:step:u |> collect
    x,y,z = collect(zeros(length(ii)) for _=1:3)
    @threads for i=1:length(ii)
        cost = ii[i]
        x[i] = cost
        y[i],z[i] = opt(max_drawdown=dd, cost=cost, n=n)
    end
    return x,y,z
end


function gen_grid(;l = 350, step=10, u=1200, n=60, glim=1.5)
    ii = l:step:u
    jj = .3:.05:glim
    final_ebitda = Dict{Tuple{Real, Real}, Real}()
    dd = Dict{Tuple{Real, Real}, Real}()
    @threads for i=1:length(ii)
        x = ii[i]
        for y in jj
            m = model(y; cost=x)
            final_ebitda[(x,y)] = ebitda(m, n)
            dd[(x,y)] = drawdown(m, n)
        end
    end
    z1(args...) = final_ebitda[args]
    z2(args...) = dd[args]
    ii, jj, z1, z2
end


function solve_target(target_ebitda; cost, months, step=.001, range=(0,3))
    o1 = 0
    o2 = 0
    g1 = 0
    g2 = 0
    for i=range[1]:step:range[2]
        m = model(i; cost=cost)
        x = ebitda(m, months)
        o2 = x
        g2 = i
        if o2 > target_ebitda
            d1 = target_ebitda - o1
            d2 = o2 - target_ebitda
            return d1 < d2 ? (g1, o1) : (g2, o2)
        end
        o1 = o2
        g1 = g2
    end
    g2, o2
end

function plot_growth_tpolicies(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    @threads for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        ys[i] = (m[1][1:months] |> totalPolicies)/1e3
    end
    p = plot(xlabel = "Months", ylabel = "Total Policies - Thousands", title="Effect of Growth Rate - Total Policies")
    for (i,g) in enumerate(growth)
        plot!(ys[i], size=(1200,900), label=g)
    end
    p
end

function plot_growth_npolicies(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    @threads for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        ys[i] = (m[1][1:months])/1e3
    end
    p = plot(xlabel = "Months", ylabel = "New Policies", title="Effect of Growth Rate - Thousands of New Policies / Month")
    for (i,g) in enumerate(growth)
        plot!(ys[i], size=(1200,900), label=g)
    end
    p
end

function plot_growth_ebitda(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        p = m[4][1:months] |> Partition(12, step=1) |> Map(v -> sum(v)/1e6) |> collect
        ys[i] = vcat(zeros(12), p)
    end
    @show ys[1] |> length
    p = plot(xlabel = "Months", title="Effect of Growth Rate - Trailing 5yr EBITDA", xticks=0:12:months, yticks=0:25:300)
    for (i,g) in enumerate(growth)
        lw = .77 < g ≤ .8 ? 4 : 1
        plot!(0:(length(ys[i])-1),ys[i], size=(1200,900), label=g, linewidth=lw)
    end
    p
end

function plot_growth_revenue(;cost, months, growth)
    n = length(growth)
    ys = [[] for _=1:n]
    @threads for i=1:n
        g = growth[i]
        m = model(g; cost=cost)
        ys[i] = m[2][1:months] |> Partition(12, step=1) |> Map(v -> sum(v)/1e6) |> collect
    end
    p = plot(xlabel = "Months", title="Effect of Growth Rate - Revenue")
    for (i,g) in enumerate(growth)
        lw = .77 < g ≤ .8 ? 4 : 1
        println(lw)
        plot!(12:(length(ys[i])+11),ys[i], size=(1200,900), label=g, linewidth=lw)
    end
    p
end

function plot_growth_drawdown(; cost, months, growth)
    n = length(growth)
    nn = length(cost)
    ys = [zeros(nn) for g in growth]
    @threads for i=1:n
        for j=1:nn
            g = growth[i]
            c = cost[j]
            m = model(g; cost=c)
            ys[i][j] = drawdown(m, months) / 1e6
        end
    end
    title = string("Effects of Growth Rate and Cost - Max Drawdown - ", months / 12, " Years")
    p = plot(xlabel = "Cost", ylabel = "Max Drawdown - Millions", title=title, size=(1200,900))
    for i=1:n
        lw = growth[i] == .785 ? 4 : :auto
        plot!(cost, ys[i], label=growth[i], seriestype=:path, linewidth=lw)
    end
    vline!([550], color=:red, linewidth=3, label="")
    p
end

function plot_growth_EBITDA(; cost, months, growth)
    n = length(growth)
    nn = length(cost)
    ys = [zeros(nn) for g in growth]
    @threads for i=1:n
        for j=1:nn
            g = growth[i]
            c = cost[j]
            m = model(g; cost=c)
            ys[i][j] = ebitda(m, months) / 1e6
        end
    end
    title = string("Effects of Growth Rate and Cost - EBITDA - ", months / 12, " Years")
    p = plot(xlabel = "Cost", ylabel = "EBITDA - Millions", title = title, size=(1200,900))
    for i=1:n
        lw = .77 < growth[i] <= .8 ? 4 : 1
        plot!(cost, ys[i], label=growth[i], linewidth=lw)
    end
    vline!([550], color=:red, linewidth=3, label="")
    p
end

s(args...;zid, months, kwargs...) = s(gen_grid(xcount=months); zid=zid, kwargs...)
function s(gr::Tuple; zid, kwargs...)
    @assert zid in (1,2,3)
    x,y,z1,z2,z3 = gr
    zfunc = (z1,z2,z3)[zid]
    z(args...) = zfunc(args...) / 1e6
    surf = surface(x,y,z, size=(1200,900); kwargs...)
end

function s1(args...; months, kwargs...)
    title = string("Cost, Growth, ", months/12,"yr EBITDA")
    ss = s(args...; months=months, zid=1, xlabel="cost", ylabel="growth", zlabel="EBITDA in millions", title=title, kwargs...)

end

function s3(args...; months, kwargs...)
    title = string("Cost, Growth, Max Drawdown")
    s(args...; months=months, zid=3, xlabel="cost", ylabel="growth", zlabel="drawdown", title=title, kwargs...)
end
