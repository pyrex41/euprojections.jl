using Transducers
using Plots



# parameters
attrition = .15
saturation_policies = 1600000 # 15% market share
starting_policies = 20000 # .17% market share
starting_annual_growth = 1 #
growth_rate_decrease = .2
saturation_growth_rate = .1
avg_cashflow = 400 # $600 /year
duration = 6 # 6 years
n_years = 4
cost_per_policy = 150+350+100
total_market_size = 3600000

use_manual_growth = false # overrides growth rate
#manual_additional_policies = [20000,30000,40000,50000,60000,70000, 80000, 90000, 100000,110000,120000, 130000]
manual_additional_policies = [20000 for _=1:12]

struct Policy
  year::Integer
  cashflow::Real
  duration::Integer
end

function payout(p::Policy, year::Integer)
  if year >= p.year
    if year <= (p.year + p.duration - 1)
      return p.cashflow
    end
  end
  return 0
end

function pcollect(i, n, d)
    p = Policy(i, avg_cashflow, duration)
    d[p.year] = (p, n)
end

function new_policies(; manual::Bool = false)
    policies = Dict{Integer, Tuple{Policy, Integer}}()
    if manual
        npolicies = manual_additional_policies
    else
        annual_growth_rates = 1:n_years |> Map(i -> starting_annual_growth - growth_rate_decrease*(i-1)) |>
            Map(x-> max(x, saturation_growth_rate)) |> collect
        @show annual_growth_rates
        npolicies = annual_growth_rates |> Scan((a,b) -> a * (1+b), starting_policies) |> collect |>
            Map(x-> min(x, saturation_policies)) |> Map(round) |> Map(Int) |> collect
    end
    1:n_years |> Map(i-> pcollect(i, npolicies[i], policies)) |> tcollect
    return policies
end

function get_cashflows(pd::Dict, inow::Integer)
    policy, n_new = pd[inow]
    revenue = foldxt(+, 1:inow |> Map(i->get_cashflows(pd, inow, i)))
    cost = n_new * cost_per_policy
    earnings = revenue - cost
    return revenue, earnings
end

function get_cashflows(pd::Dict, inow::Integer, i::Integer)
    policy, nraw = pd[i]
    cash_per_policy = payout(policy, inow)
    attrition_factor = (1 - attrition)^(inow-i)
    n = nraw * attrition_factor
    revenue = n * cash_per_policy
end

get_cashflows(pd::Dict) = 1:maximum(keys(pd)) |> Map(inow -> get_cashflows(pd, inow)) |> tcollect

function run_model(;manual::Bool = false)
  pd = new_policies(manual=manual)
  for i=1:n_years
    @show pd[i]
  end
  cf = get_cashflows(pd)
  for i in cf
    @show i
  end
  pd, cf
end

function plot_returns(pd, cf, out="out.png")
    total_market_share = total_market_size # not accounting for growth of market

    y1 = map(x->x[1]/1e6, cf) # Revenue
    pushfirst!(y1, 0)
    y2 = map(x->x[2]/1e6, cf) # Earnings
    pushfirst!(y2, 0)

    y3 = map(1:length(cf)) do i
        pd[i][2] / total_market_share
    end


    plot(y3, label="Market Share", ylabel="fraction", color=:red)
    plot!(twinx(), [y1,y2], label=["Revenue" "Earnings"], xlabel="years", ylabel="millions", legend=:bottomright, title="\$1B in < 10 Years")
    plot!(xticks=-1:1:n_years)
    savefig(out)
end
